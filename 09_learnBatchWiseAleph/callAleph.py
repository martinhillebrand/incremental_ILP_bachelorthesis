'''
Created on 17.05.2018

@author: Martin
'''
from subprocess import call,PIPE,Popen
import time
import os
import numpy as np
from prompt_toolkit import output

""" this file does the actual learning experimente"""



def call_aleph(path_bk_wo_b, path_pos_wo_f, path_neg_wo_n, path_write_rules,path_to_mode, settings):
    (minpos, i, clauselength, nodes, openlist) = settings
    
    actuallcommands  = ':-consult(aleph).\n\
    :-set(minpos,'+str(minpos)+').\n\
    :-set(i,'+str(i)+').\n\
    :-set(clauselength,'+str(clauselength)+').\n\
    :-set(nodes,'+str(nodes)+').\n\
    :-set(openlist,'+str(openlist)+').\n\
    :-read_all(\'' +path_bk_wo_b+"\',\'"+path_pos_wo_f+"\',\'"+ path_neg_wo_n +'\').\n\
    :-consult(\''+path_to_mode+'\'). \n\
    :-induce.\n\
    :-write_rules(\''+path_write_rules+'\').'
    
    proc = Popen(['yap'],  stdin=PIPE, stdout=PIPE)
    results = proc.communicate(actuallcommands)[0]    
    return results

optimised_settings = (2,2,6,5000,"inf")
this_path_to_mode = "sources/finalModes.pl"

rules_short = ["rule1","rule2","rule3"]
rules_long = ["irrelevant_rule1_hausarbeit","irrelevant_rule2_oldversion","irrelevant_rule3_picturetagging"]
rule_pairs = zip(rules_short,rules_long)
batchsizes = range(20,401+1)

for (rs,rl) in [rule_pairs[2]]:
    timefile = open("times/"+rs+".txt", "w")
    timefile.write("cumBatchSize,duration\n")
    for bs in batchsizes:        
        start = time.time()
        call_aleph(
            "sources/"+rl,
            "sources/examples/"+rs+"/batch_"+str(bs),
            "sources/examples/"+rs+"/batch_"+str(bs),
            "rules/"+rs+"_"+str(bs),
            this_path_to_mode,
            optimised_settings
            )
        end = time.time()
        dur = end-start
        timefile.write(str(bs)+","+str(dur)+"\n")
        timefile.flush()       
        print rs+"\t"+str(bs)+"\t"+str(dur)
    timefile.close()
    