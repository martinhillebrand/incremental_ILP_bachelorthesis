#coding: utf8
'''
Created on 17.05.2018

@author: Martin


This script creates the batches for Aleph
'''
import os 

for rule in ["rule1","rule2","rule3"]:
    pos = open("sources/examples/"+rule+".f","r").read().split("\n")
    neg = open("sources/examples/"+rule+".n","r").read().split("\n")
    n_pos = len(pos)
    n_neg = len(neg)
    h_pos = n_pos/(n_pos*1.0+n_neg)
    h_neg = 1-h_pos
    
    for batchsize in range(20,401+1):
        n_pos_now = int(h_pos*batchsize)
        n_neg_now = batchsize - n_pos_now
        pos_now = pos[0:n_pos_now]
        neg_now = neg[0:n_neg_now]        
        f_pos = open("sources/examples/"+rule+"/batch_"+str(batchsize)+".f","w")
        f_neg = open("sources/examples/"+rule+"/batch_"+str(batchsize)+".n","w")
        f_pos.write("\n".join(pos_now))
        f_neg.write("\n".join(neg_now))
        f_pos.close()
        f_neg.close() 
