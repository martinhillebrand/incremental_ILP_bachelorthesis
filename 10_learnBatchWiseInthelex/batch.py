#coding: utf8
'''
Created on 19.05.2018

@author: Martin
'''
import os
import time



def createParsPLFile(pf):    
    prefix = "prefix(\""+pf+"\").\n"
    fixtext = open("pars_ohne_prefix.pl").read()
    finaltext = prefix+fixtext
    return finaltext

def overwriteParsPlFile(pf):
    parspl = open("pars.pl","w")
    content = createParsPLFile(pf)
    parspl.write(content)
    parspl.close()

def deleteoldfile(prefix):
        try: 
            os.remove(prefix+"ex") 
            
        except WindowsError:
            pass
        try: 
            os.remove(prefix+"ex.old")
            
        except WindowsError:
            pass
        try: 
            os.remove(prefix+"stat")
            
        except WindowsError:
            pass        
        try: 
            os.remove(prefix+"stat.old")
            
        except WindowsError:
            pass
        
        try: 
            os.remove(prefix+"theory")
            
        except WindowsError:
            pass
        
        try: 
            os.remove(prefix+"theory.old")
            
        except WindowsError:
            pass 



def learnBatchWise(prefix):    
    #construct copy of source tun
    tunorig = open(prefix+"tun","r")
    tuncontent = tunorig.read()
    tuncopy = open(prefix+"tunCOPY","w")
    tuncopy.write(tuncontent)
    tunorig.close()
    tuncopy.close()
    
    #open source (eg ru1tun)    
    #get length of source
    tuncontent = tuncontent.split("\n")
    num_examples = len(tuncontent)
    #open recordfile
    recordfile = open("recordfiles/rec_batch_"+prefix,"w")
    recordfile.write("batchnumber\tlearningtime\ttheorylength\n")
    
    #change pars.pl
    overwriteParsPlFile(prefix)
    
    #learning cyle    
    for i in range (20,num_examples):
        #put in tun-file source[0:i] (overwrite it)
        tunorig = open(prefix+"tun","w")
        tunorig.write("\n".join(tuncontent[0:i]))
        tunorig.close()
                
        # let it run and measure time
        start =time.time()    
        os.system("inthelex.exe")        
        end = time.time()
        time_run = end-start
        print "\n\n................\nZeit:\t"+ str(time_run)
        
        #rename theory file and move it to saved theories (same with stats)
        os.rename(prefix+"theory","theories/"+prefix+"theory_batch_"+str(i))
        os.rename(prefix+"stat","stats/"+prefix+"stat_batch_"+str(i))
        #delete ex files
        os.remove(prefix+"ex")
        
        #get length of theoryfile
        theoryfile = open("theories/"+prefix+"theory_batch_"+str(i),"r")
        n_lines_theory =  len(theoryfile.readlines())
        theoryfile.close()
                
        #write results in recordfile
        recordfile.write("\t".join([str(i), str(time_run),str(n_lines_theory)])+"\n")
        recordfile.flush()
        print "\t".join([str(i), str(time_run),str(n_lines_theory)])
        
    #overwrite 
    
    recordfile.close()
    
    #reconstruct source +     delete copy
    tunorig = open(prefix+"tun","w")    
    tuncopy = open(prefix+"tunCOPY","r")
    tuncontent = tuncopy.read()
    tunorig.write(tuncontent)
    tunorig.close()
    tuncopy.close()
    os.remove(prefix+"tunCOPY")
    



for p in ["ru3"]: #"ru2" was excluded because it needed so long
    learnBatchWise(p)