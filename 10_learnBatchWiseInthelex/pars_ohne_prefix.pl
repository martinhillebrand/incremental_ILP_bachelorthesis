menu_choice(1).

partial_spec(p).
partial_gen(p).
defined_name_output_stream(user_output).
new_hypotheses(y).

maxspec([5,r,5]).
max_lit_spec(5).

maxgen(10).
max_disc_clauses(100).
min_size_rate(0.001).
choice_use_size(min_size,t,0.001).
max_size_rate(1.0).
choice_use_size(max_size,t,1.0).