'''
Created on 21.05.2018

@author: Martin
'''
#coding:utf8

'''
Created on 21.05.2018

@author: Martin
'''
import os
import pandas as pd



df = pd.DataFrame()

def readfile(filename):
    src = open("rules/"+filename,"r")
    con = src.read()
    src.close()
    n_rules = con.count("irrelevant")
    n_exp = n_rules - con.count(":-")
    n_literals = con.count("),")+con.count(").")-n_exp
    [rule,bs] = filename.split("_")    
    mode="aleph"
    return "\t".join([mode,rule,bs]) +"\t"+str(n_rules) + "\t"+ str(n_literals)+"\n"

def createEntireOutput():
    files = os.listdir("rules")
    outputfile = open("theory_analyze_results","w")
    outputfile.write("learnmethod\ttruerule\tn_examples\tn_rules_theory\tn_literals_theory\n")
    for f in files:
        line = readfile(f)
        outputfile.write(line)
        
    outputfile.close()
    
    
def sortFile():
    df = pd.read_csv("theory_analyze_results",sep="\t")
    df["n_examples"] = pd.to_numeric( df["n_examples"])
    df= df.sort_values(by=[ "learnmethod","truerule","n_examples"])
    return df



createEntireOutput()
df = sortFile()
df.to_csv("theory_analyze_results_sorted",sep="\t")

