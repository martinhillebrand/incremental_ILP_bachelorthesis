#coding:utf8

'''
Created on 21.05.2018

@author: Martin
'''
import os
import pandas as pd



df = pd.DataFrame()

def readfile(filename):
    src = open("theories_inthelex/"+filename,"r")
    con = src.read()
    src.close()
    n_rules = con.count(":-") -2
    n_literals = con.count("),")+con.count(").")-2
    [rule,mode,bs] = filename.split("_")
    rule = rule[0:3]
    rule = rule[0:2]+"le"+rule[2]
    mode="inthelex_"+mode
    return "\t".join([mode,rule,bs]) +"\t"+str(n_rules) + "\t"+ str(n_literals)+"\n"

def createEntireOutput():
    files = os.listdir("theories_inthelex")
    outputfile = open("theory_analyze_results","w")
    outputfile.write("learnmethod\ttruerule\tn_examples\tn_rules_theory\tn_literals_theory\n")
    for f in files:
        line = readfile(f)
        outputfile.write(line)
        
    outputfile.close()
    
    
def sortFile():
    df = pd.read_csv("theory_analyze_results",sep="\t")
    df["n_examples"] = pd.to_numeric( df["n_examples"])
    df= df.sort_values(by=[ "learnmethod","truerule","n_examples"])
    return df



df = sortFile()
df.to_csv("theory_analyze_results_sorted",sep="\t")
