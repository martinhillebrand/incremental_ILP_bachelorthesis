#coding:utf8
'''
Created on 21.05.2018

@author: Martin
'''
import numpy as np
import pandas as pd

finalframe = pd.DataFrame()

for m in ["batch","incremental"]:
    for r in ["ru1","ru2","ru3"]:
        rec = pd.read_csv("rec_"+m+"_"+r, sep="\t")
        cor = pd.read_csv(r+"_"+m+"_"+"correct",sep="\t")
        #print rec.head()
        #print cor.head()
        rec["decision"] = cor["decision"]
        rec["correct"] = cor["correct"]
        rec["learnmethod"] = "inthelex_"+m
        rec["truerule"] = r[:2]+"le"+r[-1]        
        #print rec.head()        
        finalframe = pd.concat([finalframe, rec])
 
            
finalframe.to_csv("alldatainthelex.txt", sep="\t")
