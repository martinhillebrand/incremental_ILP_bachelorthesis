#coding:utf8
'''
Created on 20.05.2018

@author: Martin
'''

import os
import time
from shutil import copyfile

def createParsPLFile(pf):    
    prefix = "prefix(\""+pf+"\").\n"
    fixtext = open("pars_ohne_prefix.pl").read()
    finaltext = prefix+fixtext
    return finaltext

def overwriteParsPlFile(pf):
    parspl = open("pars.pl","w")
    content = createParsPLFile(pf)
    parspl.write(content)
    parspl.close()

def deleteoldfile(prefix):
        try: 
            os.remove(prefix+"ex") 
            
        except WindowsError:
            pass
        try: 
            os.remove(prefix+"ex.old")
            
        except WindowsError:
            pass
        try: 
            os.remove(prefix+"stat")
            
        except WindowsError:
            pass        
        try: 
            os.remove(prefix+"stat.old")
            
        except WindowsError:
            pass
        
        try: 
            os.remove(prefix+"theory")
            
        except WindowsError:
            pass
        
        try: 
            os.remove(prefix+"theory.old")
            
        except WindowsError:
            pass 



def learnIncrementalWise(prefix):    
    #construct copy of source tun
    tunorig = open(prefix+"tun","r")
    tuncontent = tunorig.read()
    tuncopy = open(prefix+"tunCOPY","w")
    tuncopy.write(tuncontent)
    tunorig.close()
    tuncopy.close()
    
    #open source (eg ru1tun)    
    #get length of source
    tuncontent = tuncontent.split("\n")
    num_examples = len(tuncontent)
    #open recordfile
    recordfile = open("recordfiles/rec_incremental_"+prefix,"w")
    recordfile.write("batchnumber\tlearningtime\ttheorylength\n")
    
    #change pars.pl
    overwriteParsPlFile(prefix)
    
    #first learn batch
    tunorig = open(prefix+"tun","w")
    tunorig.write("\n".join(tuncontent[0:20]))
    tunorig.close()
    start =time.time()    
    os.system("inthelex.exe")        
    end = time.time()
    time_run = end-start
    print "\n\n................\nZeit:\t"+ str(time_run)
    copyfile(prefix+"theory","theories/"+prefix+"theory_incremental_"+str(0))
    copyfile(prefix+"stat","stats/"+prefix+"stat_incremental_"+str(0))
    theoryfile = open("theories/"+prefix+"theory_incremental_"+str(0),"r")
    n_lines_theory =  len(theoryfile.readlines())
    theoryfile.close()           
    recordfile.write("\t".join([str(0), str(time_run),str(n_lines_theory)])+"\n")
    recordfile.flush()
    print "\t".join([str(0), str(time_run),str(n_lines_theory)])

    
    #learning cycle    
    for i in range (20,num_examples):
        #put in tun-file source[i] (overwrite it)
        tunorig = open(prefix+"tun","w")
        tunorig.write(tuncontent[i])
        tunorig.close()
                
        # let it run and measure time
        start =time.time()    
        os.system("inthelex.exe")        
        end = time.time()
        time_run = end-start
        print "\n\n................\nZeit:\t"+ str(time_run)
        
        #rename theory file and move it to saved theories (same with stats)
        copyfile(prefix+"theory","theories/"+prefix+"theory_incremental_"+str(i))
        copyfile(prefix+"stat","stats/"+prefix+"stat_incremental_"+str(i))

        
        #get length of theoryfile
        theoryfile = open("theories/"+prefix+"theory_incremental_"+str(i),"r")
        n_lines_theory =  len(theoryfile.readlines())
        theoryfile.close()
                
        #write results in recordfile
        recordfile.write("\t".join([str(i), str(time_run),str(n_lines_theory)])+"\n")
        recordfile.flush()
        print "\t".join([str(i), str(time_run),str(n_lines_theory)])
        
    #overwrite 
    
    recordfile.close()
    
    #reconstruct source +     delete copy
    tunorig = open(prefix+"tun","w")    
    tuncopy = open(prefix+"tunCOPY","r")
    tuncontent = tuncopy.read()
    tunorig.write(tuncontent)
    tunorig.close()
    tuncopy.close()
    os.remove(prefix+"tunCOPY")
    deleteoldfile(prefix)
    


for p in ["ru2","ru3"]:
    learnIncrementalWise(p)

