'''
Created on 30.04.2018

@author: Martin

This Script splits the entire training set D in to D+ and D- according to each ground truth
In addition it creates the bk-file for Aleph
The order is shuffled.

'''

from pyswip import Prolog
from random import shuffle

prolog =  Prolog()
prolog.consult("newRules.pl")
prolog.consult("test_US400_1.pl")

all_examples = list(prolog.query("file(X)"))
all_examples =  [d['X'] for d in all_examples]
all_examples = list(set(all_examples))
print len(all_examples)

rulenames = ["irrelevant_rule1_hausarbeit","irrelevant_rule2_oldversion","irrelevant_rule3_picturetagging"]

for rule in rulenames:
    pos_rule =list(prolog.query(rule+"(X)"))
    pos_rule = [d['X'] for d in pos_rule]
    pos_rule = list(set(pos_rule))    
    #print str(set(pos_rule1).issubset(set(all_examples)))
    neg_rule = list(set(all_examples).difference(set(pos_rule)))
    map(shuffle,[pos_rule,neg_rule])
    filepos = open(rule+".f",'w')
    fileneg = open(rule+".n",'w')
    for pos_ex in pos_rule:
        filepos.write(rule+"("+pos_ex+").\n")
    for neg_ex in neg_rule:
        fileneg.write(rule+"("+neg_ex+").\n")
    filepos.close()
    fileneg.close()
    filebk = open(rule+".b",'w')
    bk=     ":- consult(newModes). %Aleph Modes\n\
:- consult(determination_"+rule+"). %Aleph_Determination \n \
:- consult(newRules). %Umwandlung von numerischen Attributen in diskrete Werte \n\
:- consult(test_US400_1). %eigentlicher Datensatz"
    filebk.write(bk)
    filebk.close()
    

