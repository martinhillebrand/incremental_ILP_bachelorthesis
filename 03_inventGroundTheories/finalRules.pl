
%%% RULE 1: Hausarbeit-Szenario
irrelevant_rule1_hausarbeit(X):- filename_extension_png(X).
irrelevant_rule1_hausarbeit(X):- filename_extension_tif(X).
irrelevant_rule1_hausarbeit(X):- filename_extension_ME(X).
irrelevant_rule1_hausarbeit(X):- filename_extension_xls(X).

irrelevant_rule1_hausarbeit(X):- filename_extension_pdf(X),created_longago(X).

irrelevant_rule1_hausarbeit(X):- filename_extension_doc(X),last_accessed_longago(X).
irrelevant_rule1_hausarbeit(X):- filename_extension_doc(X),last_accessed_sometimeago(X).

irrelevant_rule1_hausarbeit(X):- filename_extension_rtf(X),last_accessed_longago(X).
irrelevant_rule1_hausarbeit(X):- filename_extension_rtf(X),last_accessed_sometimeago(X).


%%% RULE 2: Many Versions-Szenario
irrelevant_rule2_oldversion(IrrFileOld) :- identical_content(IrrFileOld,NewVersion), in_same_folder(IrrFileOld,NewVersion), last_change_before(IrrFileOld,NewVersion).
irrelevant_rule2_oldversion(IrrFileOld) :- almost_identical_content(IrrFileOld,NewVersion), in_same_folder(IrrFileOld,NewVersion), last_change_before(IrrFileOld,NewVersion).

%%% RULE 3: Picture-Tagging-Szenario
irrelevant_rule3_picturetagging(X) :- filename_extension_doc(X).
irrelevant_rule3_picturetagging(X) :-filename_extension_ME(X).
irrelevant_rule3_picturetagging(X) :-filename_extension_pdf(X).
irrelevant_rule3_picturetagging(X) :- filename_extension_rtf(X).
irrelevant_rule3_picturetagging(X) :- filename_extension_txt(X).

irrelevant_rule3_picturetagging(X) :- filename_extension_png(X), size_small(X).
irrelevant_rule3_picturetagging(X) :- filename_extension_tif(X), size_small(X).

irrelevant_rule3_picturetagging(X) :- filename_extension_xls(X), in_same_folder_different_filename_extensions(X,ME), filename_extension_ME(ME).
irrelevant_rule3_picturetagging(X) :- filename_extension_xls(X),last_modified_longago(X).
irrelevant_rule3_picturetagging(X) :- filename_extension_xls(X),last_modified_sometimeago(X).