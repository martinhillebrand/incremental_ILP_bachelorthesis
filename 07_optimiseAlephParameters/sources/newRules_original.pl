same_filename_extension(F1,F2):- filename_extension(F1,Ext),filename_extension(F2,Ext),not(F1==F2).
in_same_folder(F1,F2):-in(F1,Dir),in(F2,Dir),same_filename_extension(F1,F2).
in_same_folder_different_filename_extensions(F1,F2):-in(F1,Dir),in(F2,Dir),not(F1==F2) .
created_before(F1,F2):-same_filename_extension(F1,F2), creation_time(F1,T1), creation_time(F2,T2),T1<T2.
last_change_before(F1,F2):-same_filename_extension(F1,F2), change_time(F1,T1), change_time(F2,T2),T1<T2.
last_modification_before(F1,F2):-same_filename_extension(F1,F2), modification_time(F1,T1), modification_time(F2,T2),T1<T2.
last_access_before(F1,F2):-same_filename_extension(F1,F2), access_time(F1,T1), access_time(F2,T2),T1<T2.
size_smaller(F1,F2):-same_filename_extension(F1,F2), size(F1,S1), size(F2,S2),S1<S2.
identical_content(F1,F2) :- same_filename_extension(F1,F2),dummy_dist(F1,F2,0).

filename_extension_doc(X):-filename_extension(X,'doc');filename_extension(X,'DOC').
filename_extension_ME(X):-filename_extension(X,'ME').
filename_extension_pdf(X):-filename_extension(X,'pdf');filename_extension(X,'PDF').
filename_extension_png(X):-filename_extension(X,'png');filename_extension(X,'PNG').
filename_extension_rtf(X):-filename_extension(X,'rtf'); filename_extension(X,'RTF').
filename_extension_tif(X):-filename_extension(X,'tif'); filename_extension(X,'TIF');filename_extension(X,'tiff'); filename_extension(X,'TIFF').
filename_extension_txt(X):-filename_extension(X,'txt'); filename_extension(X,'TXT').
filename_extension_xls(X):-filename_extension(X,'XLS');filename_extension(X,'xls').




created_longago(X) :- creation_time(X,Time), Time<1356998400.
created_sometimeago(X) :- creation_time(X,Time),Time>=1356998400, Time=<1498867200.
created_recently(X) :- creation_time(X,Time), Time>1498867200.

last_changed_longago(X) :- change_time(X,Time), Time<1404172800.
last_changed_sometimeago(X) :- change_time(X,Time), Time>=1404172800, Time=<1506816000.
last_changed_recently(X) :- change_time(X,Time), Time>1506816000.

last_modified_longago(X) :- modification_time(X,Time), Time<1404172800.
last_modified_sometimeago(X) :- modification_time(X,Time), Time>=1404172800, Time=<1506816000.
last_modified_recently(X) :- modification_time(X,Time), Time>1506816000.

last_accessed_longago(X) :- access_time(X,Time), Time<1451606400.
last_accessed_sometimeago(X):- access_time(X,Time), Time>=1451606400, Time=<1514764800.
last_accessed_recently(X):- access_time(X,Time), Time>1514764800.

size_small(X) :- size(X,S), S<5000.
size_medium(X) :- size(X,S), S>=5000,S=<50000.
size_large(X):- size(X,S), S>50000.

almost_identical_content(F1,F2) :- same_filename_extension(F1,F2),dummy_dist(F1,F2,Dist),Dist>0,Dist<20000.






%%% AB hier: Rles

%%% RULE 1: Hausarbeit-Szenario
irrelevant_rule1_hausarbeit(X):- filename_extension_png(X).
irrelevant_rule1_hausarbeit(X):- filename_extension_tif(X).
irrelevant_rule1_hausarbeit(X):- filename_extension_ME(X).
irrelevant_rule1_hausarbeit(X):- filename_extension_xls(X).

irrelevant_rule1_hausarbeit(X):- filename_extension_pdf(X),created_longago(X).

irrelevant_rule1_hausarbeit(X):- filename_extension_doc(X),last_accessed_longago(X).
irrelevant_rule1_hausarbeit(X):- filename_extension_doc(X),last_accessed_sometimeago(X).

irrelevant_rule1_hausarbeit(X):- filename_extension_rtf(X),last_accessed_longago(X).
irrelevant_rule1_hausarbeit(X):- filename_extension_rtf(X),last_accessed_sometimeago(X).


%%% RULE 2: Many Versions-Szenario
irrelevant_rule2_oldversion(IrrFileOld) :- identical_content(IrrFileOld,NewVersion), in_same_folder(IrrFileOld,NewVersion), last_change_before(IrrFileOld,NewVersion).
irrelevant_rule2_oldversion(IrrFileOld) :- almost_identical_content(IrrFileOld,NewVersion), in_same_folder(IrrFileOld,NewVersion), last_change_before(IrrFileOld,NewVersion).

%%% RULE 3: Picture-Tagging-Szenario
irrelevant_rule3_picturetagging(X) :- filename_extension_doc(X).
irrelevant_rule3_picturetagging(X) :-filename_extension_ME(X).
irrelevant_rule3_picturetagging(X) :-filename_extension_pdf(X).
irrelevant_rule3_picturetagging(X) :- filename_extension_rtf(X).
irrelevant_rule3_picturetagging(X) :- filename_extension_txt(X).

irrelevant_rule3_picturetagging(X) :- filename_extension_png(X), size_small(X).
irrelevant_rule3_picturetagging(X) :- filename_extension_tif(X), size_small(X).

irrelevant_rule3_picturetagging(X) :- filename_extension_xls(X), in_same_folder_different_filename_extensions(X,ME), filename_extension_ME(ME).
irrelevant_rule3_picturetagging(X) :- filename_extension_xls(X),last_modified_longago(X).
irrelevant_rule3_picturetagging(X) :- filename_extension_xls(X),last_modified_sometimeago(X).