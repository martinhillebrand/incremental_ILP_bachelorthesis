
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     Parameter Settings     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


:- modeh(1,irrelevant_rule1_hausarbeit(+origfile)).

:- consult(determination_irrelevant_rule1_hausarbeit). %Aleph_Determination 

:- consult(test_US400_1). %eigentlicher Datensatz
:- consult(newRules). %Umwandlung von numerischen Attributen in diskrete Werte 
