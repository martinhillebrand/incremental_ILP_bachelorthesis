# coding: utf8
'''
Created on 17.05.2018

@author: Martin
'''



import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

from scipy import stats


durationframe = pd.read_csv("durations")


if False:
    for myindex in ["minpos", "i", "clauselength", "nodes", "openlist"]:
        print (pd.pivot_table(durationframe, values='duration', index=["rule", myindex],
                        aggfunc=[min, max, np.mean])).to_string()




print (pd.pivot_table(durationframe, values='duration', index=["rule", "openlist"],aggfunc=[min,np.mean, max, lambda x: max(x)-min(x)])).to_string()


rule1df = durationframe[durationframe["rule"]== "irrelevant_rule1_hausarbeit"].sort_values(by=["duration"])
rule2df = durationframe[durationframe["rule"]== "irrelevant_rule2_oldversion"].sort_values(by=["duration"], ascending=False)
rule3df = durationframe[durationframe["rule"]== "irrelevant_rule3_picturetagging"].sort_values(by=["duration"])

#print rule2df.head().to_string()

if False:
    for myindex in ["minpos", "i", "clauselength", "nodes", "openlist"]:
        print (pd.pivot_table(rule2df, values='duration', index=[myindex],
                        aggfunc= lambda x: max(x)-min(x) )).to_string()





"""

Alle Ergebnisse sind mehr oder weniger identisch und gleich schnell, daher einfach default werte lassen
-> keine sinnvolle optimierung möglich


"""