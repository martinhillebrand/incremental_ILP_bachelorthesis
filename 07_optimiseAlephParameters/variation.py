# coding: utf8
from subprocess import call,PIPE,Popen
import time
import os
import numpy as np
from prompt_toolkit import output


rules = ["irrelevant_rule1_hausarbeit","irrelevant_rule2_oldversion","irrelevant_rule3_picturetagging"]
minpos_list = [2,3]
i_list = [1,2,5]
clauselength_list = [4,6] #4 tatsächliches minimum, da head mitgezehlt wird (
nodes_list = [1000,5000,10000]
openlist_list = [10,1000,"inf"]


def call_aleph(rule, bottomclause_ex, settings):
    (minpos, i, clauselength, nodes, openlist) = settings
    
    actuallcommands  = ':-consult(aleph).\n \
    :-set(minpos,'+str(minpos)+').\n\
    :-set(i,'+str(i)+').\n\
    :-set(clauselength,'+str(clauselength)+').\n\
    :-set(nodes,'+str(nodes)+').\n\
    :-set(openlist,'+str(openlist)+').\n\
    \
    :-read_all(\'sources/' +rule+'\').\n\
    :- consult(\'sources/modePerfect.pl\'). \n  ' + bottomclause_ex
    
    proc = Popen(['yap'],  stdin=PIPE, stdout=PIPE)
    results = proc.communicate(actuallcommands)[0]    
    return results


duration_file = open("durations", "w")
duration_file.write("rule,minpos,i,clauselength,nodes,openlist,duration\n")


cur = 1
max = 324
verystart = time.time()
for rule in rules:
    for minpos in minpos_list:
        for i in i_list:
            for clauselength in clauselength_list:
                for nodes in nodes_list:
                    for openlist in openlist_list:
                        theoryfilepath = "theory_"+"_".join(map(lambda x: str(x), [rule,minpos,i,clauselength,nodes,openlist]))
                        duration_line = ",".join(map(lambda x: str(x), [rule,minpos,i,clauselength,nodes,openlist]))
                        start = time.time()
                        this_result= call_aleph(rule, "induce.\n write_rules(\'resultingtheories/"+theoryfilepath+"\').", (minpos,i,clauselength,nodes,openlist))
                        end = time.time()
                        duration = end-start
                        duration_line += (","+str(duration)+"\n")         
                        duration_file.write(duration_line)
                        duration_file.flush()
                        print duration_line                        
                        print "current: " + str(cur)+ "\t of "+str(max)+"\t total time passed: "+str(end-verystart)
                        cur+=1

duration_file.close()

                        
"""
tic = time.time()

firstresult = call_aleph(rules[2], "induce.\n write_rules(\'sources/mytheory.txt\').", (2,2,4,1000,100))



tock = time.time()
duration = tock -tic
print duration
outputfile = open("firstresult.txt","w")
outputfile.write(str(firstresult))
outputfile.write("\n-------------------------------------\nDuration "+str(duration))
outputfile.close()
"""