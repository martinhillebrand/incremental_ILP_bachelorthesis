#coding: utf8
'''
Created on 19.05.2018

@author: Martin
'''
from random import shuffle

bottomclauses = open("bottomclauses.txt").readlines()
positives1 = map( lambda x: x[:-2], open("rule1.f").readlines())
positives2 = map( lambda x: x[:-2], open("rule2.f").readlines())
positives3 = map( lambda x: x[:-2], open("rule3.f").readlines())

negatives1 = map( lambda x: x[:-2], open("rule1.n").readlines())
negatives2 = map( lambda x: x[:-2], open("rule2.n").readlines())
negatives3 = map( lambda x: x[:-2], open("rule3.n").readlines())

for li in [positives1,positives2,positives3,negatives1,negatives2,negatives3]:
    #print li[-1]
    li[-1] += ")"
"""
for li in [positives1,positives2,positives3,negatives1,negatives2,negatives3]:
    print li[-1]

"""   

output_rule1 = []
output_rule2 = []
output_rule3 = []



for b in bottomclauses:
    bn = b.split(" :-")
    headr1 = bn[0]
    headr2 = headr1.replace("irrelevant_rule1_hausarbeit", "irrelevant_rule2_oldversion")
    headr3 = headr1.replace("irrelevant_rule1_hausarbeit", "irrelevant_rule3_picturetagging")
    if headr1 in positives1:
        output_rule1 += [ bn[0] + " :-"+ bn[1][:-2]]
    elif  headr1 in negatives1:
        output_rule1 +=   ["not("+ bn[0]+")"+ " :-" +bn[1][:-2]]
    else:
        print headr1 + "\tr1"
    if headr2 in positives2:
        output_rule2 += [ headr2 + " :-"+ bn[1][:-2]]
    elif headr2 in negatives2:
        output_rule2 +=   ["not("+ headr2+")"+ " :-" +bn[1][:-2]]
    else:
        print headr1  + "\tr2"
    if headr3 in positives3:
        output_rule3 += [ headr3 + " :-"+ bn[1][:-2]]
    elif headr3 in negatives3:
        output_rule3 +=   ["not("+ headr3+")"+ " :-" +bn[1][:-2]]
    else:
        print headr1  + "\tr3"


map(shuffle, [output_rule1,output_rule2,output_rule3])
"""
for o in  [output_rule1,output_rule2,output_rule3]:
    print len(o)
"""

ru1 = open("ru1tun", "w")
ru1.write("\n".join(output_rule1))
ru1.close()

ru2 = open("ru2tun", "w")
ru2.writelines("\n".join(output_rule2))
ru2.close()

ru3 = open("ru3tun", "w")
ru3.writelines("\n".join(output_rule3))
ru3.close()
