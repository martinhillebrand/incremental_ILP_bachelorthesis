prefix("o44").
menu_choice(1).

partial_spec(p).
partial_gen(p).
defined_name_output_stream(user_output).
new_hypotheses(y).

maxspec([50,r,50]).
max_lit_spec(50).

maxgen(200).
max_disc_clauses(2000).
min_size_rate(0.02).
choice_use_size(min_size,t,0.02).
max_size_rate(20.0).
choice_use_size(max_size,t,20.0).