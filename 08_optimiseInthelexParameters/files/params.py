#coding:utf8

#Parameter


#maxspec([Nodes,Type,Res]).
import os
import time
import subprocess
from subprocess import Popen, PIPE
"""
###Default Values are

maxspec([10,r,10]).
max_lit_spec(2).


maxgen(10). 
max_disc_clauses(100).
min_size_rate(0.001). 
choice_use_size(min_size,t,0.001).
max_size_rate(1.0).
choice_use_size(max_size,t,1.0).
"""

#Specialisation
maxspec_list = ["maxspec([2,r,2]).", 
                "maxspec([5,r,5]).",
                "maxspec([10,r,10]).",
                "maxspec([20,r,20]).",
                "maxspec([50,r,50])."]

max_lit_spec_list = ["max_lit_spec(2).",
                     "max_lit_spec(5).",
                     "max_lit_spec(10).",
                     "max_lit_spec(20).",
                     "max_lit_spec(50).",
                     ]
SPECIALIZATION_settings = zip(maxspec_list,max_lit_spec_list)
SPECIALIZATION_settings = map(lambda s: "\n".join(s), SPECIALIZATION_settings)


GENERLISATION_settings = [
                          "maxgen(10).\nmax_disc_clauses(100).\nmin_size_rate(0.001).\nchoice_use_size(min_size,t,0.001).\nmax_size_rate(1.0).\nchoice_use_size(max_size,t,1.0).",
                          "maxgen(20).\nmax_disc_clauses(200).\nmin_size_rate(0.002).\nchoice_use_size(min_size,t,0.002).\nmax_size_rate(2.0).\nchoice_use_size(max_size,t,2.0).",
                          "maxgen(50).\nmax_disc_clauses(500).\nmin_size_rate(0.005).\nchoice_use_size(min_size,t,0.005).\nmax_size_rate(5.0).\nchoice_use_size(max_size,t,5.0).",
                          "maxgen(100).\nmax_disc_clauses(1000).\nmin_size_rate(0.01).\nchoice_use_size(min_size,t,0.01).\nmax_size_rate(10.0).\nchoice_use_size(max_size,t,10.0).",
                          "maxgen(200).\nmax_disc_clauses(2000).\nmin_size_rate(0.02).\nchoice_use_size(min_size,t,0.02).\nmax_size_rate(20.0).\nchoice_use_size(max_size,t,20.0)."]


def createTunGraphFiles():
    o_source = open("o").read()    
    for s in range(0,5):
        for g in range (0,5):
            prefix = "o"+str(s)+str(g)        
            open("files/"+prefix+"tun","w").write(o_source) #only copying and renaming...
            open("files/"+prefix+"graph","w")


def createParsPLFile(s,g):
    prefix = "o"+str(s)+str(g)
    prefix = "prefix(\""+prefix+"\").\n"
    fixtext = "menu_choice(1).\n\npartial_spec(p).\npartial_gen(p).\ndefined_name_output_stream(user_output).\nnew_hypotheses(y).\n\n"
    finaltext = prefix+fixtext+SPECIALIZATION_settings[s]+"\n\n"+GENERLISATION_settings[g]
    return finaltext

def overwriteParsPlFile(s,g):
    parspl = open("pars.pl","w")
    content = createParsPLFile(s,g)
    parspl.write(content)
    parspl.close()

def deleteoldfile(prefix):
        try: 
            os.remove(prefix+"ex") 
            
        except WindowsError:
            pass
        try: 
            os.remove(prefix+"ex.old")
            
        except WindowsError:
            pass
        try: 
            os.remove(prefix+"stat")
            
        except WindowsError:
            pass        
        try: 
            os.remove(prefix+"stat.old")
            
        except WindowsError:
            pass
        
        try: 
            os.remove(prefix+"theory")
            
        except WindowsError:
            pass
        
        try: 
            os.remove(prefix+"theory.old")
            
        except WindowsError:
            pass 


def testParameters():
    timestats = open("timestatistics.txt","w")
    timestats.write("specSetting\tgenSetting\ttime\ttheorylength\n")
    for s in range(0,5):
        for g in range (0,5):
            overwriteParsPlFile(s,g)
            prefix = "o"+str(s)+str(g)
            deleteoldfile(prefix)        
            start =time.time()    
            os.system("inthelex.exe")        
            end = time.time()
            t = end-start        
            l = len(open(prefix+"theory").readlines())
            timetext = str(s)+"\t"+str(g)+"\t"+str(t)+"\t"+str(l)+"\n"
            timestats.write(timetext)
            timestats.flush()
            
    
    timestats.close()


print createParsPLFile(1, 0)