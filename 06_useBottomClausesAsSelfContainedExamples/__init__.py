

from subprocess import call,PIPE,Popen
import time
import os
import numpy as np

def call_aleph(rule, modeversion, bottomclause_ex):
    actuallcommands  = ':-consult(aleph).\n :-read_all(\'sources/' +rule+'\').\n\
    :- consult(\''+modeversion+'\'). \n  ' + bottomclause_ex
    
    proc = Popen(['yap'],  stdin=PIPE, stdout=PIPE)
    results = proc.communicate(actuallcommands)[0]    
    return results



rulesources = ["irrelevant_rule1_hausarbeit","irrelevant_rule2_oldversion","irrelevant_rule3_picturetagging"]

bottomclause_examples = ''.join(map(lambda bc_ind: (":-sat("+str(bc_ind)+").\n") ,range(1,11) ))


satlist = ""
for i in range(1,402):
    satlist+=":-sat("+str(i)+ ").\n"

results = call_aleph(rulesources[0], "modePerfect.pl", satlist )
result_1 = results.split("[sat]")[1:]
#print result_1[191]

for r in result_1:
    if r.find("item_312") >-1:
        print r

myindex = 0
result_1 = map(lambda r: r.split("\n"), result_1)
result_1 = map(lambda r: r[1:-3], result_1)
 
for r in result_1:
    if str(r).find("item_312") >-1:
        print "----abhier----1-"
        print "".join(r)
        myindex = result_1.index(r)

bottomclauses_heads = map(lambda r: r[0][1:-2],result_1)
bottomclauses_itemid = map(lambda head: head.split("(")[1][0:-1],bottomclauses_heads)
bottomclauses_tails = map(lambda r: r[4:],result_1)

print "----abhier----2-"
print "".join(bottomclauses_tails[myindex])
        

bottomclauses_tails = map(lambda r: "".join(r),bottomclauses_tails)
bottomclauses_tails = map(lambda tail, it_id: tail.replace("A",it_id),bottomclauses_tails,bottomclauses_itemid)
bottomclauses_tails = map(lambda tail: tail.lower(),bottomclauses_tails)
bottomclauses_tails = map(lambda tail: tail.replace("filename_extension_me","filename_extension_ME"),bottomclauses_tails)
bottomclauses_tails = map(lambda tail: tail.replace("   ",""),bottomclauses_tails)
bottomclauses_tails = map(lambda tail: tail.replace("\r"," "),bottomclauses_tails)
alltogether = map(lambda head,tail: head + " :- " +tail+"\n", bottomclauses_heads, bottomclauses_tails)


for r in alltogether:
    if str(r).find("item_312") >-1:
        print "----abhier----3-"
        print "".join(r)

open("bottomclauses.txt","w").writelines(alltogether)
