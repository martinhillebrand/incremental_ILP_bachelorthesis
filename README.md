# Incremental ILP

This project contains all the code I produced for my bachelor thesis in applied computer science.

## Abstract of the Thesis:

The objective of this study is to compare incremental and batch learning strategies of Inductive Logic Programming (ILP) in the context of the cognitive companion system Dare2Del which is supposed to make use of intentional forgetting to blend out currently irrelevant files in a data system. While batch ILP systems like Aleph (Srinivasan, 1999) always start with an empty theory and then process the entire batch of examples at once to induce a theory, incremental ILP systems like INTHELEX (Esposito, Semeraro, Fanizzi, & Ferilli, 2000) can start with an initial theory and modify it if new incoming examples are not covered. To test which of the two is better suited for Dare2Del, a simulation experiment was conducted. A fictitious data system was taken and examples of files in the data system were split into positive and negative subsets according to pre-defined ground truths. These examples were presented one after another to the learning systems, which then predicted its label using their previously induced theory. After that, the new example was added to the known data set and the learning systems relearnt or modified the theory. As assumed, the batch ILP system Aleph predicted the irrelevance of files more accurately, whereas the incremental ILP system INTHELEX was faster at relearning. Generally, Aleph showed more consistent learning durations and its theories were shorter and closer to the ground truths. From these results, Aleph should be preferred to INTHELEX. An optimal solution, however, might be a hybrid of Aleph, creating an initial theory, and INTHELEX, revising theories.

The link to the full text is [here](https://gitlab.com/martinhillebrand/incremental_ILP_bachelorthesis/blob/master/bachelorthesis_MartinHillebrand.pdf) 

## Structure of this project:

![](workflowBA.png)
