#coding: utf8
'''
Created on 18.05.2018

@author: Martin
'''
import os 
from subprocess import call,PIPE,Popen, STDOUT


myfile = open("correctness.txt","w")





#actual testing in incremental mode
for rule in ["rule1","rule2","rule3"]:
    pos = open("sources/examples/"+rule+".f","r").read().split("\n")
    neg = open("sources/examples/"+rule+".n","r").read().split("\n")
    for batchsize in range(20,401):
        
        bk = ":-consult(\'sources/test_US400_1.pl\').\n"  # background knowledge
        bt = ":-consult(\'sources/background_theory_noDef.pl\').\n" #background theory
        lr = ":-consult(\'rules/"+rule+"_"+str(batchsize)+"\').\n" # learnt rules (funktioniert auch ohne .pl)
        
        #read test example
        pos_test = open("testexamples/"+rule+"/batch_"+str(batchsize)+".f","r").read()
        neg_test = open("testexamples/"+rule+"/batch_"+str(batchsize)+".n","r").read()
       
        querry = pos_test+neg_test+"\n"
        
        actual_class = "" 
        if len(pos_test)>0: 
            actual_class= "pos"
        elif len(neg_test)>0:
            actual_class = "neg"
        
        actuallcommands = bk+ bt+lr+querry
        
        proc = Popen(['yap'],shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT)
        results= proc.communicate(actuallcommands)[0].replace("\r\n","\n").split("\n")[-2]
        
        
        
        
        #print "actually, it is " + actual_class
        #print "the prediction is ..." + str(results)
        myfile.write(rule + "\t"+ str(batchsize)+"\t")
        myfile.write(actual_class+"\t")
        myfile.write(results +"\n")
        
        #querry testexample (there is only one, either in .f or .f according to batchsize)

    
    
    #read result
    # one of for cases (TP,TN,FP,FN) -> (Correct/Incorrect)
    #write this to file with batchnumber (batchnumber\tcase\tcorrect)
    
