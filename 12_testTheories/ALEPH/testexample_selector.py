#coding: utf8
'''
Created on 18.05.2018

@author: Martin
'''
import os 
from subprocess import call,PIPE,Popen


for rule in ["rule1","rule2","rule3"]:
    
    pos = open("sources/examples/"+rule+".f","r").read().split("\n")
    neg = open("sources/examples/"+rule+".n","r").read().split("\n")
    n_pos = len(pos)
    n_neg = len(neg)
    h_pos = n_pos/(n_pos*1.0+n_neg)
    h_neg = 1-h_pos
    
    for batchsize in range(20,401):
        batchsize_test = batchsize+1
        n_pos_now = int(h_pos*batchsize)
        n_neg_now = batchsize - n_pos_now
        
        n_pos_now_test = int(h_pos*batchsize_test)
        n_neg_now_test = batchsize_test - n_pos_now_test
        
        n_test_pos = n_pos_now_test-n_pos_now
        n_test_neg = n_neg_now_test - n_neg_now
        
        pos_now_test = pos[n_pos_now:n_pos_now_test]
        neg_now_test = neg[n_neg_now:n_neg_now_test]
        
                
        f_pos_test = open("testexamples/"+rule+"/batch_"+str(batchsize)+".f","w")
        f_neg_test = open("testexamples/"+rule+"/batch_"+str(batchsize)+".n","w")
        f_pos_test.write("".join(pos_now_test))
        f_neg_test.write("".join(neg_now_test))
        f_pos_test.close()
        f_neg_test.close() 




