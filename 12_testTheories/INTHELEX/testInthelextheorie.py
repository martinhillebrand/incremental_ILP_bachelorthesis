#coding: utf8
'''
Created on 20.05.2018

@author: Martin
'''
from subprocess import call,PIPE,Popen, STDOUT
import os

#works
def outcommentFirstEightLines(pathTheoryFile):
    theoryfile = open(pathTheoryFile, "r")
    lines  = theoryfile.readlines()
    lines[0:7] = map(lambda s: "% "+s, lines[0:8])
    theoryfile.close()
    theoryfile = open(pathTheoryFile, "w")
    theoryfile.writelines(lines)
    theoryfile.close()
    
def testSingleExample(prefix, example_number, mode="batch"): # or incremental 
    #get theory
    pathTheoryFile = "theories/"+prefix+"theory_"+mode+"_"+str(example_number)
    outcommentFirstEightLines(pathTheoryFile)
    
    # get example
    pathTun = prefix +"tun"
    f = open(pathTun)
    example = f.readlines()[example_number+1].split(" :-")[0]  
    f.close()
    print example
    
    actual_val = "yes"
    if(example.startswith("not")):
        actual_val = "no"
        example = example[4:-1]
    
   
        
    #read everything into YAP (including bk + rules) #get prediction
    bk = ":-consult(\'sources/test_US400_1.pl\').\n"  # background knowledge
    bt = ":-consult(\'sources/background_theory_noDef.pl\').\n" #background theory
    lr = ":-consult(\'"+pathTheoryFile+"\').\n" # learnt rules (funktioniert auch ohne .pl)
    querry = example+".\n"
    actuallcommands = bk+ bt+lr+querry
    proc = Popen(['yap'],shell=False, stdin=PIPE, stdout=PIPE, stderr=STDOUT)
    results= proc.communicate(actuallcommands)[0].replace("\r\n","\n").split("\n")[-2]

    #compare it with actual label
    #    
    ans = ""
    if (actual_val == "yes") and (results == "yes"):
        ans = "TP"
    elif (actual_val == "yes") and (results == "no"):
        ans = "FN"
    elif (actual_val == "no") and (results == "yes"):
        ans = "FP"
    elif (actual_val == "no") and (results == "no"):        
        ans = "TN"
  
    
    #return x in {TP,TN,FP,FN}
    return ans

def testExamplesOfARun(prefix, mode="batch"):
    relevant_files = [int(x[len(prefix+"theory_"+mode)+1:]) for x in os.listdir("theories") if x.startswith(prefix+"theory_"+mode)]
    upperbound = max(relevant_files)
    output = open("validationresults/"+prefix+"_"+mode+"_correct","w")
    output.write("examplenumber\tdecision\tcorrect\n")
        
    for i in range(20,upperbound):
        ans = testSingleExample(prefix, i, mode)
        output.write("\t".join([str(i),ans,str(ans.startswith("T"))])+"\n")
        print "\t".join([prefix, mode, str(i),ans,str(ans.startswith("T"))])
        output.flush()            
    output.close()


def specialTest(): #ru3_incremental_20 mit allen aus ru3tun
    pathTheoryFile = "theories/ru3theory_incremental_20"
    
    examples = []
    pathTun = "ru3tun"
    f = open(pathTun)
    examples = [x.split(" :-")[0]  for x in  f.readlines()]
    f.close()
    labels = ["yes"   if x.startswith("not")  else "no" for x in examples ]
    examples = [x[4:-1] if x.startswith("not") else x for x in examples ]
    bk = ":-consult(\'sources/test_US400_1.pl\').\n"  # background knowledge
    bt = ":-consult(\'sources/background_theory_noDef.pl\').\n" #background theory
    lr = ":-consult(\'"+pathTheoryFile+"\').\n" # learnt rules (funktioniert auch ohne .pl)
    i = 20
    for ex in examples:
        querry = ex+".\n"
        actuallcommands = bk+ bt+lr+querry
        proc = Popen(['yap'],shell=False, stdin=PIPE, stdout=PIPE, stderr=STDOUT)
        result= proc.communicate(actuallcommands)[0].replace("\r\n","\n").split("\n")[-2]       
        print "\t".join([str(i),labels[i-2],result]) 
        i+=1
    
#specialTest()    

testSingleExample("ru1",345,mode="incremental")

"""
for p in [
    "ru1",
    #"ru2",
    #"ru3"
    ]:
    for m in [
        #"batch",
        "incremental"]:
        testExamplesOfARun(p,m)

"""