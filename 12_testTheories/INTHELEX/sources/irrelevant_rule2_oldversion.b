%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     Parameter Settings     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- modeh(1,irrelevant_rule2_oldversion(+origfile)). %andere modes werden separat eingelesen


:- consult(determination_irrelevant_rule2_oldversion). %Aleph_Determination 
 :- consult(finalRules). %Umwandlung von numerischen Attributen in diskrete Werte 
:- consult(test_US400_1). %eigentlicher Datensatz