% Unary Predicates
:- mode(*,filename_extension_doc(+origfile)).
:- mode(*,filename_extension_ME(+origfile)).
:- mode(*,filename_extension_pdf(+origfile)).
:- mode(*,filename_extension_png(+origfile)).
:- mode(*,filename_extension_rtf(+origfile)).
:- mode(*,filename_extension_tif(+origfile)).
:- mode(*,filename_extension_txt(+origfile)).
:- mode(*,filename_extension_xls(+origfile)).
:- mode(*,created_longago(+origfile)).
:- mode(*,created_sometimeago(+origfile)).
:- mode(*,created_recently(+origfile)).
:- mode(*,last_changed_longago(+origfile)).
:- mode(*,last_changed_sometimeago(+origfile)).
:- mode(*,last_changed_recently(+origfile)).
:- mode(*,last_modified_longago(+origfile)).
:- mode(*,last_modified_sometimeago(+origfile)).
:- mode(*,last_modified_recently(+origfile)).
:- mode(*,last_accessed_longago(+origfile)).
:- mode(*,last_accessed_sometimeago(+origfile)).
:- mode(*,last_accessed_recently(+origfile)).
:- mode(*,size_small(+origfile)).
:- mode(*,size_medium(+origfile)).
:- mode(*,size_large(+origfile)).

:- mode(*,filename_extension_doc(+file)).
:- mode(*,filename_extension_ME(+file)).
:- mode(*,filename_extension_pdf(+file)).
:- mode(*,filename_extension_png(+file)).
:- mode(*,filename_extension_rtf(+file)).
:- mode(*,filename_extension_tif(+file)).
:- mode(*,filename_extension_txt(+file)).
:- mode(*,filename_extension_xls(+file)).
:- mode(*,created_longago(+file)).
:- mode(*,created_sometimeago(+file)).
:- mode(*,created_recently(+file)).
:- mode(*,last_changed_longago(+file)).
:- mode(*,last_changed_sometimeago(+file)).
:- mode(*,last_changed_recently(+file)).
:- mode(*,last_modified_longago(+file)).
:- mode(*,last_modified_sometimeago(+file)).
:- mode(*,last_modified_recently(+file)).
:- mode(*,last_accessed_longago(+file)).
:- mode(*,last_accessed_sometimeago(+file)).
:- mode(*,last_accessed_recently(+file)).
:- mode(*,size_small(+file)).
:- mode(*,size_medium(+file)).
:- mode(*,size_large(+file)).


% Antisymmetric Relations --> both directions
:- mode(*,created_before(+origfile,+file)).
:- mode(*,last_change_before(+origfile,+file)).
:- mode(*,last_modification_before(+origfile,+file)).
:- mode(*,last_access_before(+origfile,+file)).
:- mode(*,size_smaller(+origfile,+file)).

:- mode(*,created_before(+file,+origfile)).
:- mode(*,last_change_before(+file,+origfile)).
:- mode(*,last_modification_before(+file,+origfile)).
:- mode(*,last_access_before(+file,+origfile)).
:- mode(*,size_smaller(+file,+origfile)).


% Symmetric Relations --> one direction
:- mode(*,identical_content(+origfile,+file)).
:- mode(*,almost_identical_content(+origfile,+file)).
:- mode(*,same_filename_extension(+origfile,+file)).

% Extending Predicate
:- mode(*,in_same_folder_different_filename_extensions(+origfile,-file)).
:- mode(*,in_same_folder(+origfile,-file)).